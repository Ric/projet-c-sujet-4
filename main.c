#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include "alpha.h"
#include "ChiffrementCesar.h"
#include "Vigenere.h"

void main() {
	struct lconv *loc;
	setlocale (LC_ALL, "");
	loc=localeconv();
	system("clear");
	wprintf(L"\n--------------------------------------\n");
	wprintf(L"-       Chiffrage de messages        -\n");
	wprintf(L"--------------------------------------\n\n");
	wprintf(L"Veuillez saisir votre message secret :\n");
	wchar_t texte[2000];
	fgetws(texte, 2000, stdin);
	getchar();
	int correct = verifAlphaNum(texte);
	while (correct ==-1) {
		wprintf(L"Message incorrect, il ne doit pas contenir de ponctuation\nRessaisissez votre message :\n");
		fgetws(texte, 2000, stdin);
		correct = verifAlphaNum(texte);
	}
	wchar_t message[2000];
	convertAccents(texte, message);
	wprintf(L"Choisissez la méthode de chiffrement : César[1], Vigenère[2]\n");
	int choix;
	wscanf(L"%d", &choix);
	getchar();
	if (choix == 1) {
		int decalage;
		wprintf(L"Saississez la clé de codage :\n");
        	wscanf(L"%d", &decalage);
		getchar();
		wprintf(L"Voulez-vous chiffrer[1] ou déchiffrer[2] le message ?\n");
	        wscanf(L"%d", &choix);
		getchar();
        	if (choix == 1) {
			chiffcesar(decalage,message);
			wprintf(L"Message chiffré:\n");
			afficherCes();
		} else if (choix == 2) {
			dechiffcesar(decalage,message);
			wprintf(L"Message déchiffré:\n");
			afficherCes();
		}
	}
		else if (choix == 2) {
		wchar_t cle[200];
        	wprintf(L"Saisissez la clé de codage (au moins deux caractères) :\n");
       		wscanf(L"%ls", &cle);
		wprintf(L"Voulez-vous chiffrer[1] ou déchiffrer[2] le message ?\n");
                wscanf(L"%d", &choix);
		getchar();
		if (choix == 1) {
                        chiffvigenere(message, cle);
                        wprintf(L"Message chiffré:\n");
						afficherVig();
                } else if (choix == 2) {
                        dechiffvigenere(message, cle);
                        wprintf(L"Message déchiffré:\n");
						afficherVig();
                }
	}
}

