#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include "Vigenere.h"

void chiffvigenere(wchar_t* message, wchar_t *clef){
	FILE * fp;
	fp = fopen("./texte.txt","w");
	int j = 0;
	for (int i=0;i<wcslen(message);i++){
		
		if (j>wcslen(clef)){
			j=0;
		}
			if (message[i] == ' ') {
                        fprintf(fp,"%c",' ');
						j--;
                }else if ((L'a'<=message[i]) && (L'z'>=message[i])) {
                        fprintf(fp,"%c",((message[i]=L'a'+((message[i]-L'a')+clef[j%(wcslen(clef))]-L'a')%26)));
		  }else{
                        fprintf(fp,"%c",((message[i]=L'A'+((message[i]-L'A')+clef[j%(wcslen(clef))]-L'A')%26)));
			}
	j++;
	}
	fclose (fp);
}
void dechiffvigenere(wchar_t* message, wchar_t *clef){
        FILE * fp;
        fp = fopen("./texte.txt","w");
		int j = 0;
        for (int i=0;i<wcslen(message);i++){
			if (j>wcslen(clef)){
				j=0;
			}
			if(message[i] == ' ') {
                        fprintf(fp,"%c",' ');
						j--;
				}else if ((L'a'<=message[i]) && (L'z'>=message[i])) {
                    wchar_t a=(message[i]-(L'a'))-(clef[j%(wcslen(clef))]-(L'a'));

                    if (a<0){
                        a+=26+(L'a');
                    }else{
                        a+=L'a';
                    }
                    fprintf(fp,"%c",a);
                }else{
                        wchar_t b=(message[i]-65)-(clef[j%(wcslen(clef))]-L'a');
                    if (b<0){
                        b+=26+65;
                    }else{
                        b+=65;
                    }
                    fprintf(fp,"%c",b);
                }
			j++;
        }
		
        fclose (fp);
}

void afficherVig(){
        wchar_t text[100];
        FILE * fp;
        fp = fopen("./texte.txt","r");
        wprintf(L"Chiffré: %ls\n",fgetws(text,100,fp));
        fclose (fp);
}
