Notre application a pour but de permettre à un utilisateur quelconque de pouvoir faire usage des méthodes de cryptage tel que le chiffrement César ou le Chiffrement Vigenère.
Binôme : LAQUERBE Henri, DE JESUS Thomas | S2B.

Le fichier `alpha.c` possède 4 fonctions : 
`int verifAlphaNum(wchar_t* texte);`
		Possède en entrée un texte en wchar_t* qui est le texte précédemment saisie par l'utilisateur lors du lancement du programme.
		Il possède une itération qui permet de vérifier caractère par caractère si le texte précédemment saisit possède un caractère spécial tel que ",;:!§/.?()_-[]|}{0123456789".
		Si le texte en contient il renvoie 1 au programme principal.		
`int dansAlphabet(wchar_t caractere);`
Prend en entrée un wchar_t désigné comme le caractère, il vérifie si le caractère en question est équivalent à un caractère du tableau alphabet, si c’est le cas il renvoie i ( c’est à dire l’indice du tableau alphabet[i] )  sinon il renvoie -1.
`int alphaNum(wchar_t caract);`
	Prend en entrée un wchar_t désigné comme le caract, il parcourt le tableau alphabet[i] et si caract est équivalent à la lettre dans le tableau il renvoie l’indice du tableau modulo 26.
`void convertAccents(wchar_t* texte, wchar_t* message);`
		Prend en entrée deux wchart_ : le texte entré par l’utilisateur et le message principalement initialisé. Il va convertir tous les accents du texte tel  que “à,é,è,ô…” et ensuite renvoie la variable message avec le texte initial corrigé.

Le fichier ` ` possède 3 fonctions:
void chiffcesar (int cle,wchar_t* texte);
Prend en entrée deux wchar_t: la clef et le message à chiffrer. Il chiffre caractère par caractère le message et l’écrit dans un fichier texte. Il peut différencier les majuscules ainsi que les espaces.
void afficherCes();
Affiche le contenu du fichier texte contenant le message chiffré ou déchiffré, selon la demande de l’utilisateur.
void dechiffcesar (int cle,wchar_t* texte);
Prend en entrée deux wchar_t: la clef et le message à déchiffrer. Il déchiffre caractère par caractère le message et l’écrit dans un fichier texte. Il peut différencier les majuscules ainsi que les espaces.

Le fichier ` ` possède 3 fonctions:
void chiffvigenere(wchar_t* message, wchar_t *clef);
	Prend en entrée deux wchar_t: la clef et le message à chiffrer. Il chiffre caractère par caractère le message et l’écrit dans un fichier texte. Il peut différencier les majuscules ainsi que les espaces.
void dechiffvigenere(wchar_t* message, wchar_t *clef);
	Prend en entrée deux wchar_t: la clef et le message à déchiffrer. Il déchiffre caractère par caractère le message et l’écrit dans un fichier texte. Il peut différencier les majuscules ainsi que les espaces.
void afficherVig();
Affiche le contenu du fichier texte contenant le message chiffré ou déchiffré, selon la demande de l’utilisateur.
 
